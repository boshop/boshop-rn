import { Navigation } from 'react-native-navigation'
import Initializing from './Initializing'
import SignIn from './SignIn'
import SignUp from './SignUp'
import TabScreen1 from './TabScreen1'
import TabScreen2 from './TabScreen2'
import TabScreen3 from './TabScreen3'
import TabScreen4 from './TabScreen4'
import Welcome from './Welcome'

export function registerScreens() {
  Navigation.registerComponent('Welcome', () => Welcome)
  Navigation.registerComponent('Initializing', () => Initializing)
  Navigation.registerComponent('SignIn', () => SignIn)
  Navigation.registerComponent('SignUp', () => SignUp)
  Navigation.registerComponent('TabScreen1', () => TabScreen1)
  Navigation.registerComponent('TabScreen2', () => TabScreen2)
  Navigation.registerComponent('TabScreen3', () => TabScreen3)
  Navigation.registerComponent('TabScreen4', () => TabScreen4)
}
