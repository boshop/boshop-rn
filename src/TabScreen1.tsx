import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default class TabScreen1 extends React.Component {
  static get options() {
    return {
      topBar: {
        title: {
          text: 'Tab Screen 1',
        },
      },
    }
  }
  public render() {
    return (
      <View style={styles.container}>
        <Text>Tab Screen 1</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
