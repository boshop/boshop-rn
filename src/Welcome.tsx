// Home.js
import { Button } from 'native-base'
import React from 'react'
import { Alert, StyleSheet, Text, View } from 'react-native'
import { Navigation } from 'react-native-navigation'

export default class Welcome extends React.Component {
  static get options() {
    return {
      topBar: {
        title: {
          text: 'Welcome',
        },
        drawBehind: true,
        visible: false,
      },
    }
  }

  public signin = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'SignIn',
      },
    })
  }

  public signup = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'SignUp',
      },
    })
  }

  public render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Hello There</Text>
        <View style={styles.button}>
          <Button rounded block info onPress={this.signin}>
            <Text style={styles.buttonText}>Sign In</Text>
          </Button>
        </View>
        <View style={styles.button}>
          <Button rounded block primary onPress={this.signup}>
            <Text style={styles.buttonText}>Register</Text>
          </Button>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#14395F',
  },
  text: {
    color: 'white',
    alignSelf: 'center',
    paddingBottom: 150,
    fontSize: 30,
  },
  button: {
    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 8,
    paddingBottom: 8,
  },
  buttonText: { color: 'white' },
})
