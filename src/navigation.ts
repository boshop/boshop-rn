// navigation.js
import { Navigation } from 'react-native-navigation'

export const goHome = () =>
  Navigation.setRoot({
    root: {
      bottomTabs: {
        id: 'BottomTabsId',
        children: [
          {
            component: {
              name: 'TabScreen1',
              options: {
                bottomTab: {
                  fontSize: 12,
                  text: 'Tab1',
                  icon: require('./signin.png'),
                },
              },
            },
          },
          {
            component: {
              name: 'TabScreen2',
              options: {
                bottomTab: {
                  text: 'Tab2',
                  fontSize: 12,
                  icon: require('./signup.png'),
                },
              },
            },
          },
          {
            component: {
              name: 'TabScreen3',
              options: {
                bottomTab: {
                  text: 'Tab3',
                  fontSize: 12,
                  icon: require('./signin.png'),
                },
              },
            },
          },
          {
            component: {
              name: 'TabScreen4',
              options: {
                bottomTab: {
                  text: 'Tab4',
                  fontSize: 12,
                  icon: require('./signup.png'),
                },
              },
            },
          },
        ],
      },
    },
  })

export const gotoWelcome = () =>
  Navigation.setRoot({
    root: {
      stack: {
        id: 'App',
        children: [
          {
            component: {
              name: 'Welcome',
            },
          },
        ],
      },
    },
  })
