// SignIn.js
import AsyncStorage from '@react-native-community/async-storage'
import React from 'react'
import { StyleSheet, Text } from 'react-native'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Button,
  View,
} from 'native-base'
import { USER_KEY } from './config'
import { goHome } from './navigation'

export default class SignIn extends React.Component {
  static get options() {
    return {
      topBar: {
        title: {
          text: 'Sign In',
        },
      },
    }
  }
  public state = {
    username: '',
    password: '',
  }

  public onChangeText = (key, value) => {
    console.log(`key: ${key}, value: ${value}`)
    this.setState({ [key]: value })
  }

  public signIn = async () => {
    try {
      await AsyncStorage.setItem(USER_KEY, 'matthew')
    } catch (error) {
      console.error('error: ' + error)
    }
    goHome()
  }

  public render() {
    return (
      <Container>
        <Header />
        <Content>
          <Form>
            <Item>
              <Input placeholder="Username" />
            </Item>
            <Item last>
              <Input placeholder="Password" />
            </Item>
            <View
              style={{
                paddingHorizontal: 20,
                paddingTop: 50,
                alignSelf: 'stretch',
              }}
            >
              <Button block primary onPress={this.signIn}>
                <Text style={{ color: 'white' }}>Sign In</Text>
              </Button>
            </View>
          </Form>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  input: {
    width: 350,
    fontSize: 18,
    fontWeight: '500',
    height: 55,
    backgroundColor: '#42A5F5',
    margin: 10,
    color: 'white',
    padding: 8,
    borderRadius: 14,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
