import AsyncStorage from '@react-native-community/async-storage'
import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { USER_KEY } from './config'
import { gotoWelcome } from './navigation'

export default class TabScreen1 extends React.Component {
  static get options() {
    return {
      topBar: {
        title: {
          text: 'Tab Screen 4',
        },
      },
    }
  }

  public signout = () => {
    AsyncStorage.removeItem(USER_KEY)
    gotoWelcome()
  }

  public render() {
    return (
      <View style={styles.container}>
        <Text>Tab Screen 4</Text>
        <Button onPress={this.signout} title="Sign Out" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
