// SignUp.js
import AsyncStorage from '@react-native-community/async-storage'
import React from 'react'
import { StyleSheet } from 'react-native'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Button,
  Text,
  View,
  Label,
} from 'native-base'
import { USER_KEY } from './config'
import { goHome } from './navigation'

export default class SignUp extends React.Component {
  static get options() {
    return {
      topBar: {
        title: {
          text: 'Sign Up',
        },
      },
    }
  }

  public state = {
    username: '',
    password: '',
    email: '',
    phone_number: '',
  }
  public onChangeText = (key: any, val: any) => {
    this.setState({ [key]: val })
  }
  public signUp = async () => {
    try {
      AsyncStorage.setItem(USER_KEY, 'matthew')
      goHome()
    } catch (error) {
      console.error('error: ' + error)
    }
  }

  public render() {
    return (
      <Container>
        <Header />
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input onChangeText={val => this.onChangeText('username', val)} />
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input onChangeText={val => this.onChangeText('password', val)} />
            </Item>
            <Item floatingLabel>
              <Label>Email</Label>
              <Input onChangeText={val => this.onChangeText('email', val)} />
            </Item>
            <Item floatingLabel>
              <Label>Phone number</Label>
              <Input
                onChangeText={val => this.onChangeText('phoneNumber', val)}
              />
            </Item>
            <View
              style={{
                paddingHorizontal: 20,
                paddingTop: 50,
                alignSelf: 'stretch',
              }}
            >
              <Button block primary onPress={this.signUp}>
                <Text style={{ color: 'white' }}>Register</Text>
              </Button>
            </View>
          </Form>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  input: {
    width: 350,
    height: 55,
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
