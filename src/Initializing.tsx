// Initializing.js
import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {USER_KEY} from './config';
import {goHome, gotoWelcome} from './navigation';

export default class Initializing extends React.Component {
  public async componentDidMount() {
    try {
      const user = await AsyncStorage.getItem(USER_KEY);
      if (user) {
        goHome();
      } else {
        gotoWelcome();
      }
    } catch (err) {
      console.error('error: ', err);
    }
  }

  public render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Loading</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  welcome: {
    fontSize: 28,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    top: 100,
  },
});
